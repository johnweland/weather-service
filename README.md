#Weather-Service#

A simple weather service using forecast.io to get weather data based on either the users current location or by geocoding an address that a user inputs.

This is part of one of my larger projects [homehub](https://github.com/johnweland/homehub). The goal is to eventually break out things like the location services to work independant of the weather-service for other odds and ends within homehub, like looking up restaurants etc.

To use:

1. Clone this repository.

	```
	git clone https://github.com/johnweland/weather-service.git
	```
2. Create an API key at [forecast.io](https://developer.forecast.io/).
3. Add a file auth.js in the repo's js folder.
4. Create a viariable for the forecate.io API key.

	```javascript
		var forecast_key = "YOUR API KEY HERE";
	```