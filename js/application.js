jQuery(document).ready(function($) {
  geo.getUserLocation();
});


var mode; 

if (mode == "gps") {
  geo.getUserLocation();
}




var geo = {
  getUserLocation: function() {
    var startPos;
    var geoOptions = {
      enableHighAccuracy: true
    }
    var geoSuccess = function(position) {
      startPos = position;
      var latlng = startPos.coords.latitude + "," + startPos.coords.longitude;
      geo.getCoodinates(latlng);
      //forecast.getForecast(address, startPos.coords.latitude, startPos.coords.longitude);
    };
    var geoError = function(error) {
      console.log('Error Code: ' + error.code +  ', \"' + error.message + '\".');
    };
    // Check if Geolocation is supported.
    if (navigator.geolocation) {
      // Is supported.
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }
    else {
      // Not supported.
      console.log('Geolocation is not supported for this Browser/OS version yet.');
    }
  },

  getCoodinates: function(address) {
    var coordinates;
    geo.geocoder.geocode({address: address}, function(results, status) {
      if (status == "OK") {
        returnAddress = results[2].formatted_address; 
        coords = results[0].geometry.location;
        coordinates = [coords.lat(), coords.lng()];
        geo.formatted_address = returnAddress;
        forecast.getForecast(returnAddress, coordinates[0], coordinates[1]);
        console.log(results);
      } else {
        console.log(status);
      }
    })
  },
  geocoder: new google.maps.Geocoder(),
  formatted_address: null
}

var forecast = {
  getForecast: function(address, latitude, longitude) {
    var url = 'https://api.forecast.io/forecast/' + forecast_key + '/' + latitude + ',' + longitude;

    $.getJSON(url + "?callback=?", function(data) {
      forecast.buildUI(data);
    });
  },

  buildUI: function(data) {
    var currently = data.currently;
    var daily = data.daily.data;
    var hourly = data.hourly;
    var count = 0;
    var icon;

    var getIcon = function(dailyIcon){
      switch (dailyIcon) {
        case 'clear-day' :
          icon = 'wi-day-sunny';
          break;      
        case 'clear-night' :
          icon = 'wi-night-clear';
          break;
        case 'rain' :
          icon = 'wi-day-rain';
          break;
        case 'snow' :
          icon = 'wi-day-snow-wind';
          break;
        case 'sleet':
          icon = 'wi-day-sleet';
          break;
        case 'wind' :
          icon = 'wi-day-windy';
          break;
        case 'fog' :
          icon = 'wi-day-fog';
          break;
        case 'cloudy' :
          icon = 'wi-day-cloudy';
          break;
        case 'partly-cloudy-day' :
          icon = 'wi-day-cloudy-high';
          break;
        case 'partly-cloudy-night' :
          icon = 'wi-night-partly-cloudy';
          break;
      }
    }

    // console.log(data); // DEBUG: Remove before launching.
    $('.current__item .location h1').text(geo.formatted_address);
    $('.current__item .icon span').text("");
    getIcon(daily[0].icon);
    $('.current__item .icon span').addClass("wi "+ icon);
    currently.temperature = Math.floor(currently.temperature);
    daily[0].temperatureMax = Math.floor(daily[0].temperatureMax);
    daily[0].temperatureMin = Math.floor(daily[0].temperatureMin);
    $('.current__item .cTemp span').text(currently.temperature + "\xB0");
    $('.current__item .hlTemp span').text(daily[0].temperatureMax + "\xB0" + " / " + daily[0].temperatureMin + "\xB0");
    $('.current__item .summary span').text(daily[0].summary);
    $.each($(".weekly__item"), function (count, value) {
      $(this).find('.icon span').text("");
      count ++;
      getIcon(daily[count].icon);
      $(this).find('.icon span').addClass('wi '+ icon);
      daily[count].temperatureMax = Math.floor(daily[count].temperatureMax);
      daily[count].temperatureMin = Math.floor(daily[count].temperatureMin);
      $(this).find('.hlTemp').text(daily[count].temperatureMax + "\xB0" + " / " + daily[count].temperatureMin + "\xB0");
      var date = new Date(daily[count].time * 1000);
      var daycount = date.getUTCDay();
      day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      $(this).find('.date').text(day[daycount]);
    });
  }
}


function toggleDrawer() {
    $('.drawer__panel').toggleClass('closed');
    if ($('.drawer__toggle > span.fa').hasClass('fa-arrow-circle-right')) {
        $('.drawer__toggle > span.fa').removeClass('fa-arrow-circle-right');
        $('.drawer__toggle > span.fa').addClass('fa-arrow-circle-left');
    } else {
        $('.drawer__toggle > span.fa').removeClass('fa-arrow-circle-left');
        $('.drawer__toggle > span.fa').addClass('fa-arrow-circle-right');
    }
}

$('body').on("click", ".drawer__toggle", toggleDrawer);